module.exports = function(grunt) {

	var fs = require('fs');
	require('load-grunt-tasks')(grunt);

	grunt.initConfig({
		config: {
			path: {
				root: '.',
				node_modules: '<%= config.path.root %>/node_modules',
				polymer_src: '<%= config.path.node_modules %>/\@polymer/polymer',

				public: '<%= config.path.root %>/public',
				elements_dest: '<%= config.path.public %>/elements',
				js_dest: '<%= config.path.public %>/js',
			},
		},

		copy: {
			polymer: {
				files: [{
					expand: true,
					flatten: true,
					src: ['<%= config.path.polymer_src %>/polymer*.html'],
					dest: '<%= config.path.elements_dest %>/',
					filter: 'isFile',
				}],
			},
            polymer_src: {
                files: [{
                    expand: true,
                    cwd: '<%= config.path.polymer_src %>/src',
                    src: ['**'],
                    dest: '<%= config.path.elements_dest %>/src',
                }],
            },
			webcomponents: {
				files: [{
					expand: true,
					flatten: true,
					src: ['<%= config.path.node_modules %>/webcomponents.js/webcomponents-lite.min.js'],
					dest: '<%= config.path.js_dest %>/',
					filter: 'isFile',
				}],
			},
		},

		clean: {
			polymer: [
                '<%= config.path.elements_dest %>/polymer*.html',
                '<%= config.path.elements_dest %>/src/',
            ],
			webcomponents: ['<%= config.path.js_dest %>/webcomponents-lite.min.js'],
		},
	});

	grunt.registerTask('build', [
		'clean',
		'copy',
	]);

}
