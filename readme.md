# Larymer

Larymer is a mashup of [Laravel](https://laravel.com/) and [Polymer](https://www.polymer-project.org/1.0/). The goal is to provide an easy way of integrating Polymer's web components into a popular and accessible PHP framework. This document is designed as a how-to for integrating Polymer components into a Laravel installation.

(While Laravel provides Elixir (a javascript task runner based on Gulp) out-of-the-box, I've found [Grunt](http://gruntjs.com/) to be easier to configure, so these instructions will use Grunt as the task runner. I've also chosen to install Homestead locally to this project to support rapid development.)

## Setup

1. Install Laravel according to the [documentation](https://laravel.com/docs/5.3#installation). I used the project name `larymer` so if you use something different, be sure to substitute that name throughout this documentation.
2. Include Homestead in the project according to the [documentation](https://laravel.com/docs/5.3/homestead#per-project-installation).
3. Start up the vagrant environment: `vagrant up`
4. Once the new environment has been provisioned, SSH into the box: `vagrant ssh`
5. `cd` into the Laravel project directory: `cd ~/larymer`

At this point in the setup, we need to install the `grunt-cli` package if it's not already installed globally:

```bash
sudo npm install -g grunt-cli
```

Now that Grunt is installed, let's pull down some additional Grunt modules that we'll use in our task runner:

```bash
npm install --save-dev grunt grunt-contrib-clean grunt-contrib-copy load-grunt-tasks @polymer/polymer
```

Let's set up our Gruntfile:

```bash
touch Gruntfile.js
```

Open `Gruntfile.js` in your preferred editor. We included `load-grunt-tasks` to make it a little easier to import our Grunt packages, so let's set it up:

```js
module.exports = function(grunt) {

    var fs = require('fs');
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({

    });
}
```

Let's take a step back here and think about what we need to have happen as we're developing. If you browse the `~/larymer/node_modules` directory you'll find a directory called `@polymer/polymer`. Inside this directory are all the files we need to get polymer running; however, because they live in the `node_modules` directory, we can't use them in Laravel quite yet. So we need a way to copy those files over to someplace we can use them. Enter `grunt-contrib-copy`. Let's configure a few paths in our `Gruntfile`:

```js
module.exports = function(grunt) {

    var fs = require('fs');
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        config: {
            path: {
                root: '.',
                node_modules: '<%= config.path.root %>/node_modules',
                polymer_src: '<%= config.path.node_modules %>/\@polymer/polymer',
            },
        },
    });
}
```

Now that we've set up our paths, let's fill out the configuration for `grunt-contrib-copy`. Polymer requires three files be available: `polymer.html`, `polymer-mini.html`, and `polymer-micro.html`. Those files, in turn, require a number of support files located in the `src` directory of the Node package we installed, so we'll need to copy that entire `src` directory over. Additionally, we need to include a polyfill for WebComponents so that older browsers can render our code. We could just manually copy these files over but we want to ensure that these files are copied over every time we compile, in case we update them at some point. So let's set up `grunt-contrib-copy` to grab those files and put them in Laravel's `/public` directory. First, in your console, set up placeholders for the `elements` directory (the `js` directory already exists):

```bash
mkdir public/elements
```

Now in your `Gruntfile`, add those paths to the `config.path` node:

```js
module.exports = function(grunt) {

    var fs = require('fs');
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        config: {
            path: {
                root: '.',
                node_modules: '<%= config.path.root %>/node_modules',
                polymer_src: '<%= config.path.node_modules %>/\@polymer/polymer',

                public: '<%= config.path.root %>/public',
                elements_dest: '<%= config.path.public %>/elements',
                js_dest: '<%= config.path.public %>/js',
            },
        },
    });
}
```

Time to do the actual configuration for `grunt-contrib-copy`:

```js
module.exports = function(grunt) {

    var fs = require('fs');
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        config: {
            path: {
                root: '.',
                node_modules: '<%= config.path.root %>/node_modules',
                polymer_src: '<%= config.path.node_modules %>/\@polymer/polymer',

                public: '<%= config.path.root %>/public',
                elements_dest: '<%= config.path.public %>/elements',
                js_dest: '<%= config.path.public %>/js',
            },
        },

        copy: {
            polymer: {
                files: [{
                    expand: true,
                    flatten: true,
                    src: ['<%= config.path.polymer_src %>/polymer*.html'],
                    dest: '<%= config.path.elements_dest %>/',
                    filter: 'isFile',
                }],
            },
            polymer_src: {
                files: [{
                    expand: true,
                    cwd: '<%= config.path.polymer_src %>/src',
                    src: ['**'],
                    dest: '<%= config.path.elements_dest %>/src',
                }],
            },
            webcomponents: {
                files: [{
                    expand: true,
                    flatten: true,
                    src: ['<%= config.path.node_modules %>/webcomponents.js/webcomponents-lite.min.js'],
                    dest: '<%= config.path.js_dest %>/',
                    filter: 'isFile',
                }],
            },
        },
    });
}

```

Here we define two groups of files to copy: `polymer`, containing our `polymer*.html` files; and `webcomponents`, containing our polyfill (which NPM included as a dependency for the `@polymer/polymer` package).

The final thing to do is add a task in grunt that will copy these files:

```js
module.exports = function(grunt) {

    var fs = require('fs');
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        config: {
            path: {
                root: '.',
                node_modules: '<%= config.path.root %>/node_modules',
                polymer_src: '<%= config.path.node_modules %>/\@polymer/polymer',

                public: '<%= config.path.root %>/public',
                elements_dest: '<%= config.path.public %>/elements',
                js_dest: '<%= config.path.public %>/js',
            },
        },

        copy: {
            polymer: {
                files: [{
                    expand: true,
                    flatten: true,
                    src: ['<%= config.path.polymer_src %>/polymer*.html'],
                    dest: '<%= config.path.elements_dest %>/',
                    filter: 'isFile',
                }],
            },
            polymer_src: {
                files: [{
                    expand: true,
                    cwd: '<%= config.path.polymer_src %>/src',
                    src: ['**'],
                    dest: '<%= config.path.elements_dest %>/src',
                }],
            },
            webcomponents: {
                files: [{
                    expand: true,
                    flatten: true,
                    src: ['<%= config.path.node_modules %>/webcomponents.js/webcomponents-lite.min.js'],
                    dest: '<%= config.path.js_dest %>/',
                    filter: 'isFile',
                }],
            },
        },
    });

    grunt.registerTask('build', [
        'copy',
    ]);
}

```

One other thing that isn't a bad idea is to remove those files prior to copying over the new files. We can do that with `grunt-contrib-clean`, then adding that task to the `build` task we defined:

```js
module.exports = function(grunt) {

    var fs = require('fs');
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        config: {
            path: {
                root: '.',
                node_modules: '<%= config.path.root %>/node_modules',
                polymer_src: '<%= config.path.node_modules %>/\@polymer/polymer',

                public: '<%= config.path.root %>/public',
                elements_dest: '<%= config.path.public %>/elements',
                js_dest: '<%= config.path.public %>/js',
            },
        },

        copy: {
            polymer: {
                files: [{
                    expand: true,
                    flatten: true,
                    src: ['<%= config.path.polymer_src %>/polymer*.html'],
                    dest: '<%= config.path.elements_dest %>/',
                    filter: 'isFile',
                }],
            },
            polymer_src: {
                files: [{
                    expand: true,
                    cwd: '<%= config.path.polymer_src %>/src',
                    src: ['**'],
                    dest: '<%= config.path.elements_dest %>/src',
                }],
            },
            webcomponents: {
                files: [{
                    expand: true,
                    flatten: true,
                    src: ['<%= config.path.node_modules %>/webcomponents.js/webcomponents-lite.min.js'],
                    dest: '<%= config.path.js_dest %>/',
                    filter: 'isFile',
                }],
            },
        },

        clean: {
            polymer: [
                '<%= config.path.elements_dest %>/polymer*.html',
                '<%= config.path.elements_dest %>/src/',
            ],
            webcomponents: ['<%= config.path.js_dest %>/webcomponents-lite.min.js'],
        },
    });

    grunt.registerTask('build', [
        'clean',
        'copy',
    ]);
}
```

Run your task from the command line:

```bash
grunt build
```

If all goes well, you should see output similar to the following:

```
Running "clean:polymer" (clean) task
>> 0 paths cleaned.

Running "clean:webcomponents" (clean) task
>> 0 paths cleaned.

Running "copy:polymer" (copy) task
Copied 3 files

Running "copy:polymer_src" (copy) task
Created 11 directories, copied 76 files

Running "copy:webcomponents" (copy) task
Copied 1 file

Done.
```

If you `cd` into the `public/elements` directory you should see your `polymer*.html` files, and if you `cd` into the `public/js` directory, you should see your `webcomponents-lite.min.js` file.

Just to make sure the `grunt-contrib-clean` plugin is working, go back to the `~/larymer` directory and run `grunt build` again. The output should change slightly:

```
Running "clean:polymer" (clean) task
>> 4 paths cleaned.

Running "clean:webcomponents" (clean) task
>> 1 path cleaned.

Running "copy:polymer" (copy) task
Copied 3 files

Running "copy:polymer_src" (copy) task
Created 11 directories, copied 76 files

Running "copy:webcomponents" (copy) task
Copied 1 file

Done.
```

And that concludes the easy part.

## Creating the Template

For the rest of the guide, I'm going to assume you're familiar with creating new [routes](https://laravel.com/docs/5.3/routing) and using the [Blade templating system](https://laravel.com/docs/5.3/blade). If not, click through those links and read up before continuing.

Also, from here on out, all paths will be relative to the `~/larymer` directory.

First thing is to create a new template. Create `resources/views/layout.blade.php` and populate it with a basic HTML template. Just the bare bones will be fine for the purposes of this guide:

```html
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
    </head>
    <body>
        <h1>Larymer</h1>
        @yield('content')
    </body>
</html>
```

Alter your `/` route to point to this new view for now:

```php
Route::get('/', function () {
    return view('layout');
});
```

Browse to `http://homestead.app` in your browser (assuming you didn't change the Homestead defaults) and you should see your basic page.

There's one other thing we should do now, before we get too far, and that's include the WebComponents.js polyfill. Since we use `grunt-contrib-copy` to copy that file over to `public/js`, we can just create an asset link to that file directly:

```html
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        {{-- Include the WebComponents polyfill --}}
        <script src="{{ asset('/js/webcomponents-lite.min.js') }}"></script>
    </head>
    <body>
        <h1>Larymer</h1>
        @yield('content')
    </body>
</html>
```

### Creating a Polymer template

According to [the docs](https://www.polymer-project.org/1.0/start/first-element/step-2) a custom Polymer element should include, at a minimum, a reference to `polymer.html`. One of the benefits of using the Blade templating engine is its support for inheritance. So let's create a basic template that we can use when we create our own Polymer element. Create a new file, `resources/views/polymer-template.blade.php`, and fill it out like this:

```html
<link rel="import" href="{{ asset('/elements/polymer.html') }}">
```

We're not done yet. We want to make this something we can use with the [`@extends()`](https://laravel.com/docs/5.3/blade#extending-a-layout) directive in Blade, so let's create a `@section` that we can populate:

```html
<link rel="import" href="{{ asset('/elements/polymer.html') }}">

@yield('dom-module')
```

The name of the section (in this case, `dom-module`) isn't important, as long as you use it in your custom elements. I chose the name because `dom-module` is the HTML tag that Polymer uses to encapsulate the element.

After all that bootstrapping, we're finally ready to create our element! Create a new file, `resources/views/custom-element.blade.php`, and populate it like so:

```html
@extends('polymer-element')

@section('dom-module')
    <dom-module id="custom-element">

    </dom-module>
@endsection
```

Can you see where we're going with this? When this template is completely rendered, we'll have everything we need for a custom Polymer element, by virtue of the fact that we're extending our basic Polymer template.

Side note: is it worth this much effort just to include one `<link>` tag at the top of our template? Maybe not. But if you're building out a large app, and you need to make sure that all your custom elements inherit the same set of Polymer pre-built elements (for instance, you want to include a bunch of [Material Design](https://elements.polymer-project.org/) components), this will save you a lot of typing. And if you decide to add more, you no longer need to go through elements one at a time and add the new component to each file; simply add it to the template, and the new component will be propagated to all the elements that extend that template.

For now, though, let's just finish building out our element. Suppose we want a section header: a piece of content with a specific ID, based on the text to be displayed. Let's further suppose that this piece of content should have a specific style. Lastly, let's assume that we want to use this kind of section header all throughout our application, which is why we want to create a component for it. Our markup might look something like this:

```html
<h1 id="about" class="section-header">About</h1>
<h1 id="contact-us" class="section-header">Contact Us</h1>
```

```css
h1.section-header {
    color: #f00;
}
```

Let's turn that into a component. First and easiest thing is to tackle the markup:

```html
@extends('polymer-element')

@section('dom-module')
    <dom-module id="custom-element">
        <template>
            <h1 class="section-header">[[slug]]</h1>
        </template>
    </dom-module>
@endsection
```

Astute readers will notice the `<h1>` is missing an `id`. Let's add that programmatically.

```html
@extends('polymer-element')

@section('dom-module')
    <dom-module id="custom-element">
        <template>
            <h1 class="section-header">[[slug]]</h1>
        </template>
        <script>
            Polymer({
                is: 'custom-element',
                properties: {
                    slug: String,
                },
                ready: function () {
                    var tag = this.getElementsByTagName("h1")[0],
                        transformedSlug = this.slug.replace(" ", "-").toLowerCase();

                    tag.id = transformedSlug;
                }
            });
        </script>
    </dom-module>
@endsection
```

You can see that I'm transforming the `slug` variable that Polymer receives and setting the ID dynamically.

Now let's add the styling that we want:

```html
@extends('polymer-element')

@section('dom-module')
    <dom-module id="custom-element">
        <style>
            :host .section-header {
                color: #f00;
            }
        </style>
        <template>
            <h1 class="section-header">[[slug]]</h1>
        </template>
        <script>
            Polymer({
                is: 'custom-element',
                properties: {
                    slug: String,
                },
                ready: function () {
                    var tag = this.getElementsByTagName("h1")[0],
                        transformedSlug = this.slug.replace(" ", "-").toLowerCase();

                    tag.id = transformedSlug;
                }
            });
        </script>
    </dom-module>
@endsection
```

Now we need a good way to render these templates. We can't just copy them into the `public/elements` folder like we did with the `polymer*.html` files; they contain Blade-specific directives and need to be rendered. So we'll have to create a new route to retrieve them. Open up your `routes.php` file and add a new route:

```php
Route::get('/getElement/{elementName}', ['as' => 'getElement', 'uses' => function ($elementName) {
    return view($elementName);
}]);
```

Last thing we need to do is to clean up our layout to make sure that it's not called directly, and that it's flexible enough to include our new components. Let's start with the second goal first. Edit your `resources/views/layout.blade.php` to include a place where we can load our custom elements:

```html
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        {{-- Include the WebComponents polyfill --}}
        <script src="{{ asset('/js/webcomponents-lite.min.js') }}"></script>

        {{-- Include any custom elements here --}}
        @yield('elements')
    </head>
    <body>
        <h1>Larymer</h1>
        @yield('content')
    </body>
</html>
```

Now let's create a main page so we can stop modifying our default layout. Create `resources/views/main.blade.php` and fill it in like this:

```html
@extends('layout')

@section('elements')
<link rel="import" href="{{ route('getElement', ['elementName' => 'custom-element']) }}">
@append

@section('content')
<custom-element slug="About"></custom-element>
<custom-element slug="Contact Us"></custom-element>
@endsection
```

Modify your `routes.php` file again, this time adjusting the `/` route to show our `main.blade.php` template, rather than just `layout.blade.php`:

```php
Route::get('/', function () {
    return view('main');
});
```

Fire up `http://homestead.app` in your browser again. Now you should see your custom elements. And that's the tutorial, folks!

This isn't really a tutorial on Laravel or Polymer specifically, rather one way you can join both packages and use them hand-in-hand. But the savvy Laravel developer will see some additional ways this project can be optimized and organized; and likewise the experienced Polymer developer can probably find ways to stream in [Vulcanize](https://github.com/Polymer/vulcanize) or build in additional Material Design elements. Hopefully this project has given you a starting point, though.
