@extends('layout')

@section('elements')
<link rel="import" href="{{ route('getElement', ['elementName' => 'custom-element']) }}">
@append

@section('content')
<custom-element slug="About"></custom-element>
<custom-element slug="Contact Us"></custom-element>
@endsection
